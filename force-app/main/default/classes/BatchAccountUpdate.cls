global class BatchAccountUpdate implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        
        String query = 'Select id, name from Account';
        return Database.getQueryLocator(query);
        }
    global void execute(Database.BatchableContext BC, List<Account> scope)  
    {
        for(Account a:scope)
        {
            a.Name=a.Name + '*';
            
        }
        update scope;
        
    }
    global void finish(Database.BatchableContext BC)  
    {        
        
    }
}
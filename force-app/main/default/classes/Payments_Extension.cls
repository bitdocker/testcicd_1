public class Payments_Extension {
//payment__c record variable to hold record information
    public Payment__c thisPayment {get;set;}
	public Statement__c thisStatement {get;set;}
    public Map<Id, String> recordTypeMap {get;set;}
	public boolean success {get;set;}
    
    public Payments_Extension(ApexPages.StandardController scon)
    {
      if(scon.getRecord() instanceOf Statement__c)
      {
          thisStatement =[Select id,name,Rental_Agreement__r.renter__c, Rental_Agreement__r.balance__c from Statement__c where id =:scon.getId()];        
      }
        success =false;
        recordTypeMap =new Map<Id, String>();
        //query for all the record types in payment object
        //and we will store it in the Map
        for(RecordType r:[Select id, name from RecordType where sobjecttype ='Payment__c'])
        {
            recordTypeMap.put(r.id, r.name);           
        }
        //Instantiate the payment object
        thisPayment =new Payment__c();
        if(scon.getRecord() instanceOf Statement__c)
        {
          thisPayment.Statement__c = scon.getId();
          thisPayment.Amount__c = thisStatement.Rental_Agreement__r.balance__c;         
        }
        //If guest user, preset type to credit card
        if(getIsGuest())
        {
            for(id i: recordTypeMap.keySet())
            {
                if(recordTypeMap.get(i) =='Credit Card')
                {
                    thisPayment.RecordTypeId=i;
                }
            }
        }
        else if(scon.getRecord() instanceOf Statement__c)
        {
            Contact renter=[select id,firstName,lastName,mailingStreet,mailingCity,mailingState,mailingPostalCode from Contact where  id =:thisStatement.Rental_Agreement__r.Renter__c];
        	thisPayment.Billing_Name__c= renter.FirstName + '' + renter.LastName;
            thisPayment.Billing_Street__c= renter.MailingStreet;
             thisPayment.Billing_City__c= renter.MailingCity;
             thisPayment.Billing_State__c= renter.MailingState;
             thisPayment.Billing_Postal_Code__c= renter.MailingPostalCode;
            
        }
            
    }   
    
    //will process and save our payments 
    //or report any errors in the attempt
    
	public PageReference savePayment()
    {
        success=false;
        String paymentType=recordTypeMap.get(thisPayment.RecordTypeId);
        
        //validate
        if(validateFields(paymentType))
        {
            //process credit card payments
            if(paymentType=='Credit Card')
            {
                //create a request wrapper for authorize.net
                API_authorizeDotNet.authnetReq_Wrapper req=new  API_authorizeDotNet.authnetReq_Wrapper();
                //set the wrapper values
                req.amt=string.valueOf(thisPayment.Amount__c);
                req.firstName=(thisPayment.Billing_Name__c.contains(' ')?thisPayment.Billing_Name__c.subStringbefore(' '):thisPayment.Billing_Name__c);
                req.lastName=(thisPayment.Billing_Name__c.contains(' ')?thisPayment.Billing_Name__c.subStringafter(' '):thisPayment.Billing_Name__c);
                req.billstreet=thisPayment.Billing_Street__c;
                req.billcity=thisPayment.Billing_City__c;
				req.billstate=thisPayment.Billing_State__c;
				req.billzip=thisPayment.Billing_Postal_Code__c;
                
                //Set credit card information on the request wrapper
                req.ccnum=thisPayment.Credit_Card_Number__c;
                req.ccexp=monthmap.get(thisPayment.Credit_Card_Expiration_Month__c)+thisPayment.Expiration_Year__c;
                req.ccsec=thisPayment.Credit_Card_Security_Card__c;
                
                //Give this req a name
                req.ordername='Payment of '+[select id, name from Statement__c where id=:thisPayment.Statement__c].name;
                //Process our authorize.net request
                API_authorizeDotNet.AuthnetResp_Wrapper res =API_authorizeDotNet.authdotnetcharge(req);
                thisPayment.Authorize_net_Transaction_ID__c=res.TransactionID;
                thisPayment.Authorize_net_Authorization_Code__c=res.AuthorizationCode;
                thisPayment.Authorize_net_Response__c=res.responseCode + '|' +res.ResponseReasonText;
                
                //if the transaction failed
                if(res.responseCode!='1' || res.responseReasonText!='This Transaction has been approved')
                {
                 thisPayment.Status__c='Failed'; 
                      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Payment Failed'));
                      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'res.ResponseReasonText'));
                    
                    return null;
                }
                
            }
            
            //Successful Transaction
            thisPayment.Status__c='Paid';
            thisPayment.Payment_Date__c=System.now();
            upsert thisPayment;
            success=true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Payment Successful'));
            try
            {
             //If there is an email rpovided by the renter
             if(thisStatement!=null && thisStatement.Rental_Agreement__r.renter__r.email!=null)  
             {
             //Construct my message
             Messaging.SingleEmailMessage msg =new Messaging.SingleEmailMessage();
                 msg.setToAddresses(new List<String> {thisStatement.Rental_Agreement__r.renter__r.email});
                 msg.setSubject('Payment Confirmation');
                 msg.setHtmlBody('Your Payment of'+thisPayment.Amount__c+' has been successfully processed. <br/><br/> Thank You!!');
                 msg.setPlainTextBody('Your Payment of'+thisPayment.Amount__c+' has been successfully processed. \n \n Thank You!!');
                 
                 //send the email
                 Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {msg});
             }
            }
            catch(Exception e)
            {
                
            }
        }      
            else
            {
             ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please fill out all the details'));               
            }
             return null;                     
        }
    	public boolean validateFields(string paymentType)
        {
            boolean valid=true;
            //check common fields
            if(thisPayment.Statement__c==null)
                valid=false;
            if(string.isblank(thisPayment.Billing_Name__c))
                valid=false;
            if(string.isblank(thisPayment.Billing_Street__c))
                valid=false;
            if(string.isblank(thisPayment.Billing_City__c))
                valid=false;
            if(string.isblank(thisPayment.Billing_State__c))
                valid=false;
            if(string.isblank(thisPayment.Billing_Postal_Code__c))
                valid=false;
            
            //Check specific fields related to the payment type
            if(PaymentType=='Check')
            {
            if(string.isblank(thisPayment.Check_Account_Number__c))
                valid=false;
            if(string.isblank(thisPayment.Check_Routing_Number__c))
                valid=false;
            }
            else if(PaymentType=='Credit Card')
            {
             if(string.isblank(thisPayment.Credit_Card_Number__c))
                valid=false;
            if(string.isblank(thisPayment.Credit_Card_Expiration_Month__c))
                valid=false;  
            if(string.isblank(thisPayment.Expiration_Year__c))
                valid=false;  
            if(string.isblank(thisPayment.Credit_Card_Security_Card__c))
                valid=false;      
            }
            return valid;            
        }
    public boolean getIsGuest()
    {
       return [select id,userType from Profile where id=:userInfo.getProfileId()].userType=='Guest';       
    }
    
    public static Map<String,String> monthmap=new Map<String,String>{
        'January'=>'01',
        'February'=>'02',
        'March'=>'03',    
        'April'=>'04',    
        'May'=>'05',
        'June'=>'06',
        'July'=>'07',
        'August'=>'08',
        'September'=>'09',
        'October'=>'10',    
        'November'=>'11',    
        'December'=>'12'
        };
            
		public list<selectoption> getPaymentRecordTypes()
        {
            list<selectOption> temp=new list<selectOption>();
            temp.add(new selectoption('','Select Payment Method'));
            for(id i:recordTypeMap.keySet()) 
            {
                temp.add(new SelectOption(i,recordTypeMap.get(i)));
            }
            return temp;    
        }
    
    		public list<selectOption>  getExpirationYears()
            {
              List<selectOption>  temp=new List<SelectOption>();
                for(integer i=0;i<5;i++)
                {
                    String y='' +System.today().addYears(i).year();
                    temp.add(new selectOption(y,y));
                }
                return temp;
            }               
	}